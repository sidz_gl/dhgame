// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DHObjectiveMarker.generated.h"

class USphereComponent;

UCLASS()
class DHGAME_API ADHObjectiveMarker : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADHObjectiveMarker();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(VisibleAnywhere, BlueprintDefaultsOnly, Category = "Components")
	USphereComponent *m_SphereComp;
	UPROPERTY(VisibleAnywhere,BlueprintDefaultsOnly, Category = "Components")
	UStaticMeshComponent *m_StaticMeshComp;

	void NotifyActorBeginOverlap(AActor *a_otherActor) override;
	


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
