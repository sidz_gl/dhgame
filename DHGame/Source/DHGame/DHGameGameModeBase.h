// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DHGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DHGAME_API ADHGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
