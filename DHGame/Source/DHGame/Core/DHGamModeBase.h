// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DHGamModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DHGAME_API ADHGamModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
