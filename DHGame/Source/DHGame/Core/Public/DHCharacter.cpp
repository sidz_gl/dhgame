// Fill out your copyright notice in the Description page of Project Settings.


#include "DHCharacter.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
// Sets default values
ADHCharacter::ADHCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	m_CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	m_CameraComp->bUsePawnControlRotation = true;

	m_SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringComponent"));

	m_SpringArmComp->SetupAttachment(RootComponent);
	m_CameraComp->SetupAttachment(m_SpringArmComp);


}

// Called when the game starts or when spawned
void ADHCharacter::BeginPlay()
{
	Super::BeginPlay();

}

void ADHCharacter::Move(float a_fDelta)
{
	AddMovementInput(GetActorForwardVector()*a_fDelta);
}

void ADHCharacter::MoveRight(float a_fDelta)
{
	AddMovementInput(GetActorRightVector()*a_fDelta);
}

// Called every frame
void ADHCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ADHCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveUp", this, &ADHCharacter::Move);
	PlayerInputComponent->BindAxis("MoveRight", this, &ADHCharacter::MoveRight);

	PlayerInputComponent->BindAxis("LookUp", this, &ADHCharacter::AddControllerPitchInput);//Up and Down
	PlayerInputComponent->BindAxis("Turn", this, &ADHCharacter::AddControllerYawInput);//Around



}

