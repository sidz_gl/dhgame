// Fill out your copyright notice in the Description page of Project Settings.


#include "DHObjectiveMarker.h"
#include "Components/SphereComponent.h"

// Sets default values
ADHObjectiveMarker::ADHObjectiveMarker()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	m_StaticMeshComp = CreateDefaultSubobject <UStaticMeshComponent>(TEXT("StaticMeshComp"));

	m_SphereComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	m_SphereComp->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	m_SphereComp->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	

}

void ADHObjectiveMarker::NotifyActorBeginOverlap(AActor *a_otherActor)
{
	
}
// Called when the game starts or when spawned
void ADHObjectiveMarker::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADHObjectiveMarker::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

