// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DHGame/DHObjectiveMarker.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDHObjectiveMarker() {}
// Cross Module References
	DHGAME_API UClass* Z_Construct_UClass_ADHObjectiveMarker_NoRegister();
	DHGAME_API UClass* Z_Construct_UClass_ADHObjectiveMarker();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_DHGame();
// End Cross Module References
	void ADHObjectiveMarker::StaticRegisterNativesADHObjectiveMarker()
	{
	}
	UClass* Z_Construct_UClass_ADHObjectiveMarker_NoRegister()
	{
		return ADHObjectiveMarker::StaticClass();
	}
	struct Z_Construct_UClass_ADHObjectiveMarker_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADHObjectiveMarker_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_DHGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADHObjectiveMarker_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DHObjectiveMarker.h" },
		{ "ModuleRelativePath", "DHObjectiveMarker.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADHObjectiveMarker_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADHObjectiveMarker>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADHObjectiveMarker_Statics::ClassParams = {
		&ADHObjectiveMarker::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ADHObjectiveMarker_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ADHObjectiveMarker_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADHObjectiveMarker()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADHObjectiveMarker_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADHObjectiveMarker, 1685892250);
	template<> DHGAME_API UClass* StaticClass<ADHObjectiveMarker>()
	{
		return ADHObjectiveMarker::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADHObjectiveMarker(Z_Construct_UClass_ADHObjectiveMarker, &ADHObjectiveMarker::StaticClass, TEXT("/Script/DHGame"), TEXT("ADHObjectiveMarker"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADHObjectiveMarker);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
