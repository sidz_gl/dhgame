// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DHGame/Core/Public/DHCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDHCharacter() {}
// Cross Module References
	DHGAME_API UClass* Z_Construct_UClass_ADHCharacter_NoRegister();
	DHGAME_API UClass* Z_Construct_UClass_ADHCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_DHGame();
	DHGAME_API UFunction* Z_Construct_UFunction_ADHCharacter_Move();
	DHGAME_API UFunction* Z_Construct_UFunction_ADHCharacter_MoveRight();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
// End Cross Module References
	void ADHCharacter::StaticRegisterNativesADHCharacter()
	{
		UClass* Class = ADHCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Move", &ADHCharacter::execMove },
			{ "MoveRight", &ADHCharacter::execMoveRight },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ADHCharacter_Move_Statics
	{
		struct DHCharacter_eventMove_Parms
		{
			float a_fDelta;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_a_fDelta;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADHCharacter_Move_Statics::NewProp_a_fDelta = { "a_fDelta", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DHCharacter_eventMove_Parms, a_fDelta), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADHCharacter_Move_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADHCharacter_Move_Statics::NewProp_a_fDelta,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADHCharacter_Move_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Core/Public/DHCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADHCharacter_Move_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADHCharacter, nullptr, "Move", sizeof(DHCharacter_eventMove_Parms), Z_Construct_UFunction_ADHCharacter_Move_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADHCharacter_Move_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADHCharacter_Move_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADHCharacter_Move_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADHCharacter_Move()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADHCharacter_Move_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADHCharacter_MoveRight_Statics
	{
		struct DHCharacter_eventMoveRight_Parms
		{
			float a_fDelta;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_a_fDelta;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADHCharacter_MoveRight_Statics::NewProp_a_fDelta = { "a_fDelta", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DHCharacter_eventMoveRight_Parms, a_fDelta), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADHCharacter_MoveRight_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADHCharacter_MoveRight_Statics::NewProp_a_fDelta,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADHCharacter_MoveRight_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Core/Public/DHCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADHCharacter_MoveRight_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADHCharacter, nullptr, "MoveRight", sizeof(DHCharacter_eventMoveRight_Parms), Z_Construct_UFunction_ADHCharacter_MoveRight_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADHCharacter_MoveRight_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADHCharacter_MoveRight_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADHCharacter_MoveRight_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADHCharacter_MoveRight()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADHCharacter_MoveRight_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ADHCharacter_NoRegister()
	{
		return ADHCharacter::StaticClass();
	}
	struct Z_Construct_UClass_ADHCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_SpringArmComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_SpringArmComp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_CameraComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_CameraComp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADHCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_DHGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ADHCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ADHCharacter_Move, "Move" }, // 2407287798
		{ &Z_Construct_UFunction_ADHCharacter_MoveRight, "MoveRight" }, // 988276432
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADHCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "Core/Public/DHCharacter.h" },
		{ "ModuleRelativePath", "Core/Public/DHCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADHCharacter_Statics::NewProp_m_SpringArmComp_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Core/Public/DHCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADHCharacter_Statics::NewProp_m_SpringArmComp = { "m_SpringArmComp", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADHCharacter, m_SpringArmComp), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADHCharacter_Statics::NewProp_m_SpringArmComp_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADHCharacter_Statics::NewProp_m_SpringArmComp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADHCharacter_Statics::NewProp_m_CameraComp_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Core/Public/DHCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADHCharacter_Statics::NewProp_m_CameraComp = { "m_CameraComp", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADHCharacter, m_CameraComp), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADHCharacter_Statics::NewProp_m_CameraComp_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADHCharacter_Statics::NewProp_m_CameraComp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ADHCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADHCharacter_Statics::NewProp_m_SpringArmComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADHCharacter_Statics::NewProp_m_CameraComp,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADHCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADHCharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADHCharacter_Statics::ClassParams = {
		&ADHCharacter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ADHCharacter_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_ADHCharacter_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ADHCharacter_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ADHCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADHCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADHCharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADHCharacter, 1307167795);
	template<> DHGAME_API UClass* StaticClass<ADHCharacter>()
	{
		return ADHCharacter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADHCharacter(Z_Construct_UClass_ADHCharacter, &ADHCharacter::StaticClass, TEXT("/Script/DHGame"), TEXT("ADHCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADHCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
