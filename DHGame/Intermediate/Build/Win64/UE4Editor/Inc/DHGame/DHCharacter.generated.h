// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DHGAME_DHCharacter_generated_h
#error "DHCharacter.generated.h already included, missing '#pragma once' in DHCharacter.h"
#endif
#define DHGAME_DHCharacter_generated_h

#define DHGame_Source_DHGame_Core_Public_DHCharacter_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execMoveRight) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_a_fDelta); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MoveRight(Z_Param_a_fDelta); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMove) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_a_fDelta); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Move(Z_Param_a_fDelta); \
		P_NATIVE_END; \
	}


#define DHGame_Source_DHGame_Core_Public_DHCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execMoveRight) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_a_fDelta); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MoveRight(Z_Param_a_fDelta); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMove) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_a_fDelta); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Move(Z_Param_a_fDelta); \
		P_NATIVE_END; \
	}


#define DHGame_Source_DHGame_Core_Public_DHCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADHCharacter(); \
	friend struct Z_Construct_UClass_ADHCharacter_Statics; \
public: \
	DECLARE_CLASS(ADHCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DHGame"), NO_API) \
	DECLARE_SERIALIZER(ADHCharacter)


#define DHGame_Source_DHGame_Core_Public_DHCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesADHCharacter(); \
	friend struct Z_Construct_UClass_ADHCharacter_Statics; \
public: \
	DECLARE_CLASS(ADHCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DHGame"), NO_API) \
	DECLARE_SERIALIZER(ADHCharacter)


#define DHGame_Source_DHGame_Core_Public_DHCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADHCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADHCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADHCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADHCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADHCharacter(ADHCharacter&&); \
	NO_API ADHCharacter(const ADHCharacter&); \
public:


#define DHGame_Source_DHGame_Core_Public_DHCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADHCharacter(ADHCharacter&&); \
	NO_API ADHCharacter(const ADHCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADHCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADHCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADHCharacter)


#define DHGame_Source_DHGame_Core_Public_DHCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__m_CameraComp() { return STRUCT_OFFSET(ADHCharacter, m_CameraComp); } \
	FORCEINLINE static uint32 __PPO__m_SpringArmComp() { return STRUCT_OFFSET(ADHCharacter, m_SpringArmComp); }


#define DHGame_Source_DHGame_Core_Public_DHCharacter_h_11_PROLOG
#define DHGame_Source_DHGame_Core_Public_DHCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	DHGame_Source_DHGame_Core_Public_DHCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	DHGame_Source_DHGame_Core_Public_DHCharacter_h_14_RPC_WRAPPERS \
	DHGame_Source_DHGame_Core_Public_DHCharacter_h_14_INCLASS \
	DHGame_Source_DHGame_Core_Public_DHCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define DHGame_Source_DHGame_Core_Public_DHCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	DHGame_Source_DHGame_Core_Public_DHCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	DHGame_Source_DHGame_Core_Public_DHCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	DHGame_Source_DHGame_Core_Public_DHCharacter_h_14_INCLASS_NO_PURE_DECLS \
	DHGame_Source_DHGame_Core_Public_DHCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DHGAME_API UClass* StaticClass<class ADHCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID DHGame_Source_DHGame_Core_Public_DHCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
