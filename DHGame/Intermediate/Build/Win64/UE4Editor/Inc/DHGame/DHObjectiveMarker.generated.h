// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DHGAME_DHObjectiveMarker_generated_h
#error "DHObjectiveMarker.generated.h already included, missing '#pragma once' in DHObjectiveMarker.h"
#endif
#define DHGAME_DHObjectiveMarker_generated_h

#define DHGame_Source_DHGame_DHObjectiveMarker_h_12_RPC_WRAPPERS
#define DHGame_Source_DHGame_DHObjectiveMarker_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define DHGame_Source_DHGame_DHObjectiveMarker_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADHObjectiveMarker(); \
	friend struct Z_Construct_UClass_ADHObjectiveMarker_Statics; \
public: \
	DECLARE_CLASS(ADHObjectiveMarker, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DHGame"), NO_API) \
	DECLARE_SERIALIZER(ADHObjectiveMarker)


#define DHGame_Source_DHGame_DHObjectiveMarker_h_12_INCLASS \
private: \
	static void StaticRegisterNativesADHObjectiveMarker(); \
	friend struct Z_Construct_UClass_ADHObjectiveMarker_Statics; \
public: \
	DECLARE_CLASS(ADHObjectiveMarker, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DHGame"), NO_API) \
	DECLARE_SERIALIZER(ADHObjectiveMarker)


#define DHGame_Source_DHGame_DHObjectiveMarker_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADHObjectiveMarker(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADHObjectiveMarker) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADHObjectiveMarker); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADHObjectiveMarker); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADHObjectiveMarker(ADHObjectiveMarker&&); \
	NO_API ADHObjectiveMarker(const ADHObjectiveMarker&); \
public:


#define DHGame_Source_DHGame_DHObjectiveMarker_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADHObjectiveMarker(ADHObjectiveMarker&&); \
	NO_API ADHObjectiveMarker(const ADHObjectiveMarker&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADHObjectiveMarker); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADHObjectiveMarker); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADHObjectiveMarker)


#define DHGame_Source_DHGame_DHObjectiveMarker_h_12_PRIVATE_PROPERTY_OFFSET
#define DHGame_Source_DHGame_DHObjectiveMarker_h_9_PROLOG
#define DHGame_Source_DHGame_DHObjectiveMarker_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	DHGame_Source_DHGame_DHObjectiveMarker_h_12_PRIVATE_PROPERTY_OFFSET \
	DHGame_Source_DHGame_DHObjectiveMarker_h_12_RPC_WRAPPERS \
	DHGame_Source_DHGame_DHObjectiveMarker_h_12_INCLASS \
	DHGame_Source_DHGame_DHObjectiveMarker_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define DHGame_Source_DHGame_DHObjectiveMarker_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	DHGame_Source_DHGame_DHObjectiveMarker_h_12_PRIVATE_PROPERTY_OFFSET \
	DHGame_Source_DHGame_DHObjectiveMarker_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	DHGame_Source_DHGame_DHObjectiveMarker_h_12_INCLASS_NO_PURE_DECLS \
	DHGame_Source_DHGame_DHObjectiveMarker_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DHGAME_API UClass* StaticClass<class ADHObjectiveMarker>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID DHGame_Source_DHGame_DHObjectiveMarker_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
