// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DHGAME_DHGamModeBase_generated_h
#error "DHGamModeBase.generated.h already included, missing '#pragma once' in DHGamModeBase.h"
#endif
#define DHGAME_DHGamModeBase_generated_h

#define DHGame_Source_DHGame_Core_DHGamModeBase_h_15_RPC_WRAPPERS
#define DHGame_Source_DHGame_Core_DHGamModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define DHGame_Source_DHGame_Core_DHGamModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADHGamModeBase(); \
	friend struct Z_Construct_UClass_ADHGamModeBase_Statics; \
public: \
	DECLARE_CLASS(ADHGamModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/DHGame"), NO_API) \
	DECLARE_SERIALIZER(ADHGamModeBase)


#define DHGame_Source_DHGame_Core_DHGamModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesADHGamModeBase(); \
	friend struct Z_Construct_UClass_ADHGamModeBase_Statics; \
public: \
	DECLARE_CLASS(ADHGamModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/DHGame"), NO_API) \
	DECLARE_SERIALIZER(ADHGamModeBase)


#define DHGame_Source_DHGame_Core_DHGamModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADHGamModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADHGamModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADHGamModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADHGamModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADHGamModeBase(ADHGamModeBase&&); \
	NO_API ADHGamModeBase(const ADHGamModeBase&); \
public:


#define DHGame_Source_DHGame_Core_DHGamModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADHGamModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADHGamModeBase(ADHGamModeBase&&); \
	NO_API ADHGamModeBase(const ADHGamModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADHGamModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADHGamModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADHGamModeBase)


#define DHGame_Source_DHGame_Core_DHGamModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define DHGame_Source_DHGame_Core_DHGamModeBase_h_12_PROLOG
#define DHGame_Source_DHGame_Core_DHGamModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	DHGame_Source_DHGame_Core_DHGamModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	DHGame_Source_DHGame_Core_DHGamModeBase_h_15_RPC_WRAPPERS \
	DHGame_Source_DHGame_Core_DHGamModeBase_h_15_INCLASS \
	DHGame_Source_DHGame_Core_DHGamModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define DHGame_Source_DHGame_Core_DHGamModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	DHGame_Source_DHGame_Core_DHGamModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	DHGame_Source_DHGame_Core_DHGamModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	DHGame_Source_DHGame_Core_DHGamModeBase_h_15_INCLASS_NO_PURE_DECLS \
	DHGame_Source_DHGame_Core_DHGamModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DHGAME_API UClass* StaticClass<class ADHGamModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID DHGame_Source_DHGame_Core_DHGamModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
