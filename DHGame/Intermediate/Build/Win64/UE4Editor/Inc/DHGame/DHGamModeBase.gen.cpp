// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DHGame/Core/DHGamModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDHGamModeBase() {}
// Cross Module References
	DHGAME_API UClass* Z_Construct_UClass_ADHGamModeBase_NoRegister();
	DHGAME_API UClass* Z_Construct_UClass_ADHGamModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_DHGame();
// End Cross Module References
	void ADHGamModeBase::StaticRegisterNativesADHGamModeBase()
	{
	}
	UClass* Z_Construct_UClass_ADHGamModeBase_NoRegister()
	{
		return ADHGamModeBase::StaticClass();
	}
	struct Z_Construct_UClass_ADHGamModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADHGamModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DHGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADHGamModeBase_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Core/DHGamModeBase.h" },
		{ "ModuleRelativePath", "Core/DHGamModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADHGamModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADHGamModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADHGamModeBase_Statics::ClassParams = {
		&ADHGamModeBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002A8u,
		METADATA_PARAMS(Z_Construct_UClass_ADHGamModeBase_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ADHGamModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADHGamModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADHGamModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADHGamModeBase, 3297764451);
	template<> DHGAME_API UClass* StaticClass<ADHGamModeBase>()
	{
		return ADHGamModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADHGamModeBase(Z_Construct_UClass_ADHGamModeBase, &ADHGamModeBase::StaticClass, TEXT("/Script/DHGame"), TEXT("ADHGamModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADHGamModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
